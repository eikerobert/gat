# README #

A script that chooses the ssh key for git depending on commandline arguments or the output of git remote -v, if inside a git repository

Steps to install:

Find and replace the strings myaccount, myuser and mykey with the corresponding values
and alter the url if needed. Run

ln -s -T /path/to/gat /somewhere/in/your/path/git

Actually the new symbolic link must come before /usr/bin/ in your path so that the script is run whenever you type git in your shell.
to replace your native git
Caution: You must ensure to always call the true git with /usr/bin/git inside this script to avoid an infinite recursion!
If it seems safer for you run

ln -s -T /path/to/gat /somewhere/in/your/path/gat

to get a new command named gat.
